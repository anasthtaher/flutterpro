import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'fetshdata.dart';
import 'wetherjson.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: wethlist(),
      color: Colors.orange,
    );
  }
}

List city = [
  "amman",
  "hebron",
  "london",
  "cairo",
  "Abu Dhabi",
  "Tunis",
  "Ankara",
  "Beirut",
  "Berlin"
];

class wethlist extends StatefulWidget {
  @override
  _wethlistState createState() => _wethlistState();
}

class _wethlistState extends State<wethlist> {
  // List city = ["amman", "hebron", "london", "cairo","Abu Dhabi","Tunis","Ankara","Beirut","Berlin"];
  List<wether> weth = [];
  wether ww;
  wether ss;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          "wether🌤",
          style: TextStyle(color: Colors.orange),
        ),
      ),
      backgroundColor: Colors.black,
      body: Column(
        children: [
          Expanded(
              child: ListView.builder(
            itemCount: city.length,
            itemBuilder: (context, index) {
              // ss=WeatherRepo().getWeather(city[index]) as wether;

              return Padding(
                padding: const EdgeInsets.all(3.0),
                child: ListTile(
                  tileColor: Colors.orange[300],
                  leading: Text(
                    city[index],
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  trailing: FutureBuilder(
                    future: WeatherRepo().getWeather(city[index]),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return IconButton(
                          onPressed: () {
                            ww = snapshot.data;

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => second(ww),
                                )
                            );
                          },
                          icon: Icon(
                            Icons.blur_on_outlined,
                            color: Colors.black,
                          ),
                          color: Colors.grey,
                        );
                      } else
                        return Text('');
                    },
                  ),
                ),
              );
            },
          )),
        ],
      ),
    );
  }
}

class second extends StatefulWidget {
  final wether ww;

  second(this.ww);

  @override
  _MyAppState createState() => _MyAppState(ww);
}

class _MyAppState extends State<second> {
  final wether ww;

  _MyAppState(this.ww);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("weather City"),
      ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width,
            color: Colors.black,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Text(
                    "Weather",
                    style: TextStyle(
                        color: Colors.orange,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Text(ww.temp != null ? ww.temp.toString() + " C" : "loading...",
                    style: TextStyle(
                        color: Colors.yellow,
                        fontSize: 40,
                        fontWeight: FontWeight.w600)),
                // Padding(
                //   padding: EdgeInsets.only(top: 10),
                //   child: Text(
                //     ww.corrently != null
                //         ? ww.corrently.toString() + "\u0000"
                //         : "loading...",
                //     style: TextStyle(
                //         color: Colors.white,
                //         fontSize: 14,
                //         fontWeight: FontWeight.w600),
                //   ),
                // ),
                // Text("city name:${city[index]}"),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(25),
              child: ListView(
                children: [
                  ListTile(
                    tileColor: Colors.black,
                    leading: FaIcon(
                      FontAwesomeIcons.thermometerHalf,
                      color: Colors.red,
                    ),
                    title: Text(
                      "tempretsher",
                      style: TextStyle(color: Colors.teal),
                    ),
                    trailing: Text(
                      ww.temp != null
                          ? ww.temp.toString() + " C"
                          : "loading...",
                      style: TextStyle(color: Colors.lightGreenAccent),
                    ),
                    hoverColor: Colors.blue,
                  ),
                  // ListTile(
                  //   leading: FaIcon(FontAwesomeIcons.cloud),
                  //   title: Text("clud"),
                  //   trailing: Text(description != null
                  //       ? description + "\u0000"
                  //       : "loading..."),
                  // ),
                  ListTile(
                    tileColor: Colors.black,
                    leading: Icon(
                      Icons.wb_sunny,
                      color: Colors.yellowAccent,
                    ),
                    title: Text(
                      "sun",
                      style: TextStyle(color: Colors.teal),
                    ),
                    trailing: Text(
                      ww.humidity != null
                          ? ww.humidity.toString() + "\u0000"
                          : "loading...",
                      style: TextStyle(color: Colors.lightGreenAccent),
                    ),
                  ),
                  ListTile(
                    tileColor: Colors.black,
                    leading: FaIcon(
                      FontAwesomeIcons.temperatureHigh,
                      color: Colors.blue,
                    ),
                    title: Text(
                      "pressure",
                      style: TextStyle(color: Colors.teal),
                    ),
                    trailing: Text(
                      ww.pressure != null
                          ? ww.pressure.toString() + "\u0000"
                          : "loading...",
                      style: TextStyle(color: Colors.lightGreenAccent),
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {


                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //       builder: (context) => therd(),
                      //     )
                      // );
                    },
                    child: Text("see more "),
                    color: Colors.lightGreen,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}






















































